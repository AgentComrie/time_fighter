﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace TimeFighter
{
    class FinishScreen : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Text gameName;
        private Text startPrompt;
        private Game1 game;
        private Level ourLevel;

        // ------------------
        // Behaviour
        // ------------------
        public FinishScreen(Game1 newGame)
        {
            game = newGame;
        }
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            SpriteFont titleFont = content.Load<SpriteFont>("largeFont");
            SpriteFont smallFont = content.Load<SpriteFont>("mainFont");

            gameName = new Text(titleFont);
            gameName.SetTextString("You Win!");
            gameName.SetAlignment(Text.Alignment.CENTRE);
            gameName.SetColor(Color.White);
            gameName.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 300));

            startPrompt = new Text(smallFont);
            startPrompt.SetTextString("[Press R to Restart Game or ESC To Exit Game!]");
            startPrompt.SetAlignment(Text.Alignment.CENTRE);
            startPrompt.SetColor(Color.White);
            startPrompt.SetPosition(new Vector2(graphics.Viewport.Bounds.Width / 2, 200));
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            gameName.Draw(spriteBatch);
            startPrompt.Draw(spriteBatch);
            spriteBatch.End();
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            //Check if the player has pressed enter

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // If the player has pressed enter.....

            if (keyboardState.IsKeyDown(Keys.R))
            {
                //TODO: CHANGE TO LEVEL SCREEN

                game.ChangeScreen("level");

            }
        }
        // ------------------
    }
}
