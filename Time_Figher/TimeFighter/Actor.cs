﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeFighter
{
    class Actor : AnimatingSprite
    {
        // ------------------
        // Data
        // ------------------
        protected Vector2 velocity = new Vector2(100, 100);


        // ------------------
        // Behaviour
        // ------------------
        public Actor(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFramesPerSecond)
            : base(newTexture, newFrameWidth, newFrameHeight, newFramesPerSecond) // Passes the information up to the parent (base) class (Sprite).
        {
            // ACTOR STUFF
        }
        // ------------------
        public override void Update(GameTime gameTime) // override so that it properly replaces the AnimatingSprite Update function
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update position based on velocity and frame time
            position += velocity * frameTime;

            // Make sure our AnimatingSprite still updates
            base.Update(gameTime);
        }
    }
}
