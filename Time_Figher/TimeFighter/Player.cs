﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace TimeFighter
{
    class Player : Actor
    {
        // ------------------
        // Data
        // ------------------
        private Level levelObject;
        private int lives = 3;
        private Door ourPortal;
        private int score = 0;

        private bool isDead = false;
        private Rectangle rectangle;

        private int tileX;
        private int tileY;
        private int tileHeight;
        private int tileWidth;

        // Constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;


        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFramesPerSecond, Level newLevelObject, int newHealth)
            : base(newTexture, newFrameWidth, newFrameHeight, newFramesPerSecond)
        {
            // STUFF
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            PlayAnimation("walkDown");

            levelObject = newLevelObject;
           
        }
        //-------------------------
        public override void Update(GameTime gameTime)
        {
            //float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 position = Vector2.Zero;
            Vector2 prevPosition = Vector2.Zero;
            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -0.5f;
                PlayAnimation("walkLeft");

            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 0.5f;
                PlayAnimation("walkRight");

              

            }
            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -0.5f;
                PlayAnimation("walkUp");

              
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 0.5f;
                PlayAnimation("walkDown");

                
            }

            // Add the movement change to the velocity
            velocity += movementInput * MOVE_ACCEL * deltaTime;

            // Apply drag from the ground
            velocity *= MOVE_DRAG_FACTOR;

            // If the speed is too high, clamp it to a reasonable max
            if (velocity.Length() > MAX_MOVE_SPEED)
            {
                velocity.Normalize();
                velocity *= MAX_MOVE_SPEED;
            }

            // If the player isn't moving, stop the animation
            if (velocity.Length() < MIN_ANIM_SPEED)
            {
                StopAnimation();
            }

            position += velocity * deltaTime;
            prevPosition = position;


            base.Update(gameTime);
        }
        //-------------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(playerBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        // ------------------
        public void HandleCollision(Door hitPortal)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitPortal.GetCollisionDepth(playerBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        // ------------------
        public void HandleCollision(Enemy hitEnemy)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitEnemy.GetCollisionDepth(playerBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        // ------------------
        public Vector2 GetPosition()
        {
            return position;
        }
        // ------------------
        public void Kill()
        {
            levelObject.ResetLevel();

        }
        public int GetScore()
        {
            return score;
        }
    }
}
