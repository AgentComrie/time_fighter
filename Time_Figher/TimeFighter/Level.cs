﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace TimeFighter
{
    class Level : Screen
    {
        // ------------------
        // Data
        // ------------------
        private Player player;
        private Door ourdoor;
        private Wall[,] walls;
        private Tile[,] floorTiles;
        private Tile[,] tiles;

        private List<Enemy> enemies = new List<Enemy>();

        // Level Related 
        private const int FINAL_LEVEL = 3;
        private bool loadNextLevel = false;

        private bool isDead = false;

        private const float END_SCREEN_COOLDOWN = 0.25f;

        private int tileWidth;
        private int tileHeight;
        private int levelWidth;
        private int levelHeight;
        private Text scoreDisplay;
        private int currentLevel;
        private bool reloadLevel = false;

        private Game1 game;
        private Camera ourCamera;

        // My games assets
        private Texture2D playerSprite;
        private Texture2D enemySprite;
        private Texture2D doorSprite;
        private Texture2D wallSprite;
        private Texture2D floorSprite;

        private SoundEffect gameWin;
        private Song gameBackground;
        private SoundEffectInstance winGameInstance;

        private Vector2 newPosition;
        private int timeSinceDeath;


        // ------------------
        // Behaviour
        // ------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            //Game Audio Variables
            gameWin = content.Load<SoundEffect>("victory"); // Load the sound effect for winning the game.
            gameBackground = content.Load<Song>("music"); // This is the ambient sound of the game as it plays.

            winGameInstance = gameWin.CreateInstance(); // This is the instance for the player win sfx


            //Foreground Sprites
            playerSprite = content.Load<Texture2D>("PlayerAnimation");
            enemySprite = content.Load<Texture2D>("EnemySprite");
            wallSprite = content.Load<Texture2D>("Wall2");
            doorSprite = content.Load<Texture2D>("doorO");
            //Background Sprites
            floorSprite = content.Load<Texture2D>("Floor");


            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;



            // Create the player once, we'll move them later
            player = new Player(playerSprite, tileWidth, tileHeight, 6, this, 100);
            ourdoor = new Door(doorSprite);

            // Create our camera
            ourCamera = new Camera(player, new Vector2(Game1.ScreenWidth,
                Game1.ScreenHeight));

            MediaPlayer.Play(gameBackground);

            // TEMP - this will be moved later
            LoadLevel(1);
        }
        // ------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "Levels/level ";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }
        // ------------------
        public Level(Game1 newGame)
        {
            game = newGame;
        }
        public void LoadLevel(string fileName)
        {

            // Clear any existing level data
            ClearLevel();

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line " + lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array

            tiles = new Tile[lineWidth, numLines];
            floorTiles = new Tile[lineWidth, numLines];

            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < numLines; ++y)
            {
                for (int x = 0; x < lineWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }

        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Player
                case 'P':
                    CreatePlayer(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Enemy
                case 'E':
                    CreateEnemy(tileX, tileY);
                    CreateFloor(tileX, tileY);
                    break;

                // Door
                case 'D':
                    CreateDoor(tileX, tileY);
                    break;

                // Blank space
                case '.':
                    CreateFloor(tileX, tileY);
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol " + tileType + " at line " + tileY + " and character " + tileX);
            }
        }
        // ------------------
     
        private void CreateFloor(int tileX, int tileY)
        {
            Floor tile = new Floor(floorSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            floorTiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall tile = new Wall(wallSprite);
            tile.SetTilePosition(new Vector2(tileX, tileY));
            tiles[tileX, tileY] = tile;
        }
        // ------------------
        private void CreateDoor(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX, tileY);
            ourdoor.SetPosition(tilePosition);
            ourdoor.SetPlayer(player);
        }
        // ------------------
        private void CreatePlayer(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX, tileY);
            player.SetPosition(tilePosition);
        }
        // ------------------
        private void CreateEnemy(int tileX, int tileY)
        {
            Enemy newEnemy = new Enemy(enemySprite, this, 0, newPosition);
            Vector2 tilePosition = new Vector2(tileX, tileY);
            newEnemy.SetPosition(tilePosition);
            enemies.Add(newEnemy);
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Update all actors

            ourCamera.Update();
            player.Update(gameTime);

            ourdoor.Update(gameTime);




            // if the player has died then display the end screen  
            if (isDead == true)
            {
                timeSinceDeath = 0;
                game.ChangeScreen("end");
            }

            if (isDead == true && (keyboardState.IsKeyDown(Keys.R)))
            {
                reloadLevel = true;
                isDead = false;

            }

            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }

            foreach (Enemy eachEnemy in enemies)
            {
                if (eachEnemy != null)
                    eachEnemy.Update(gameTime);
            }


            // Collision checking

            // Walls collisions
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBounds());
            foreach (Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
            }


            // Enemy collisions
            foreach (Enemy eachEnemy in enemies)
            {
                if (!reloadLevel && eachEnemy.GetVisible() == true && eachEnemy.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachEnemy);
                }

            }



            // Door Collisions
            if (!reloadLevel && ourdoor.GetisVisible() == true && ourdoor.GetBounds().Intersects(player.GetBounds()))
            {
                player.HandleCollision(ourdoor);
                Victory();
            }


            // Update score
            scoreDisplay.SetTextString("Score: " + player.GetScore());


            // Check if we need to reload the level
            if (reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }

            // If we were waiting to load a new level, do it now
            if (loadNextLevel == true)
            {
                if (currentLevel == FINAL_LEVEL)
                {
                    LoadLevel(1); // restart so they can load again
                    game.ChangeScreen("finish");
                }
                else
                {
                    LoadLevel(currentLevel + 1);
                }
                loadNextLevel = false;
            }
        }
        // --------------------
        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> tilesInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        //---------------------
        public Wall GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        // ------------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            //Draw The Camera
            ourCamera.Begin(spriteBatch);

            //Draw The Floors Tiles
            foreach (Tile tile in floorTiles)
            {
                if (tile != null)
                    tile.Draw(spriteBatch);
            }

            //Draw The Player
            player.Draw(spriteBatch);

            //Draw The Portal When The Scroll Is Collected
            if (ourdoor.GetisVisible() == true)
            {
                ourdoor.Draw(spriteBatch);
            }
            //Drawing The Walls
            foreach (Wall eachWall in walls)
            {
                if (eachWall != null)
                    eachWall.Draw(spriteBatch);
            }

            //Drawing Enemies
            foreach (Enemy eachEnemy in enemies)
            {
                if (eachEnemy != null)
                    eachEnemy.Draw(spriteBatch);
            }

            //End Of Draw Function
            spriteBatch.End();

        }
        //----------------------
        public void ResetLevel()
        {
            // Delay reloading level until after the update loop
            reloadLevel = true;
            isDead = false;
        }
        // ------------------
        public void Victory()
        {
            player.Kill();
            loadNextLevel = true;
            ourdoor.SetIsVisible(false);

        }
        // ------------------
        private void ClearLevel()
        {
          enemies.Clear();
        }
        // ------------------
    }
}
