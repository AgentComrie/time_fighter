﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TimeFighter
{
    class Camera
    {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Vector2 viewSize;
        private Vector2 position;
        private Player target;


        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Camera(Player newTarget, Vector2 newViewSize)
        {
            target = newTarget;
            viewSize = newViewSize;
        }
        // -------------------------------------
        public void Update()
        {
            position.X = target.GetPosition().X + 0.1f * viewSize.X;
            position.Y = target.GetPosition().Y + 0.3f * viewSize.Y;
        }
        // -------------------------------------
        public void Begin(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(transformMatrix: Matrix.CreateTranslation(-position.X, -position.Y, 0) * Matrix.CreateTranslation(Game1.ScreenWidth / 2, Game1.ScreenHeight / 2, 0));
        }
        // -------------------------------------
    }
}
