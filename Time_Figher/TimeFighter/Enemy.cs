﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;


namespace TimeFighter
{
    class Enemy : Tile
    {
        private Level levelObject;
        private Player ourPlayer;

        private int tileX;
        private int tileY;
        private int tileHeight;
        private int tileWidth;

        // Constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;

        // ------------------
        // Behaviour
        // ------------------
        public Enemy(Texture2D newTexture, Level newLevelObject, float newDistance, Vector2 newPosition)//,
            : base(newTexture)
        {
            //Data
            
            position = newPosition;
            levelObject = newLevelObject;

        }
        // -------------------------------------
        public Vector2 GetCollisionDepth(Rectangle otherBounds)
        {
            // This function calculates how far our rectangles are overlapping
            Rectangle tileBounds = GetBounds();

            // Calculate the half sizes of both rectangles
            float halfWidthEnemy = otherBounds.Width / 2.0f;
            float halfHeightEnemy = otherBounds.Height / 2.0f;
            float halfWidthTile = tileBounds.Width / 2.0f;
            float halfHeightTile = tileBounds.Height / 2.0f;

            // Calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfWidthEnemy,
                                                otherBounds.Top + halfHeightEnemy);
            Vector2 centreTile = new Vector2(tileBounds.Left + halfWidthTile,
                                             tileBounds.Top + halfHeightTile);

            // How far away are the centres of each of these rectangles from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            // Minimum distance these need to be to NOT collide / intersect
            // If EITHER the X or the Y distance is greater than these minima, these are NOT intersecting
            float minDistanceX = halfWidthEnemy + halfWidthTile;
            float minDistanceY = halfHeightEnemy + halfHeightTile;

            // If we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // Calculate and return the intersection depth
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        // ------------------
        public void Kill()
        {
            visible = false;
        }
        // -------------------------------------
    }
}
