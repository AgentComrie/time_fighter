﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace TimeFighter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch; 

        public static int ScreenHeight;
        public static int ScreenWidth;

        // variables 
        Texture2D playerSprite;


        Dictionary<string, Screen> screens = new Dictionary<string, Screen>();
        Screen currentScreen = null;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
             graphics.PreferredBackBufferHeight = 1080;
             graphics.PreferredBackBufferWidth = 1920;

            graphics.ApplyChanges();


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            playerSprite = Content.Load<Texture2D>("PlayerAnimation");

            Level level = new Level(this);
            level.LoadContent(Content, GraphicsDevice);
            screens.Add("level", level);

            TitleScreen title = new TitleScreen(this);
            title.LoadContent(Content, GraphicsDevice);
            screens.Add("title", title);

            EndScreen end = new EndScreen(this);
            end.LoadContent(Content, GraphicsDevice);
            screens.Add("end", end);

            FinishScreen finish = new FinishScreen(this);
            finish.LoadContent(Content, GraphicsDevice);
            screens.Add("finish", finish);

            currentScreen = title;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        public void ChangeScreen(string screenName)
        {
            //Check and make sure our dictionary actually contains this key
            // Before attempting to access it (otherwise we crash)
            if (screens.ContainsKey(screenName))
            {
                // The screen DOES exist
                // Set the current screen to it
                currentScreen = screens[screenName];
            }
            // TODO: use an assert or exception if the key is not in the dictionary
        }
    }
}
