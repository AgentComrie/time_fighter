﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Diagnostics;


namespace TimeFighter
{
    class AnimatingSprite : Sprite
    {
        // ------------------
        // Types
        // ------------------
        struct Animation
        {
            public int startFrame;
            public int endFrame;
        }


        // ------------------
        // Data
        // ------------------

        // Setting
        private int frameWidth;
        private int frameHeight;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();
        private float framesPerSecond;

        // Runtime
        private int currentFrame;
        private float timeInFrame;
        private string currentAnimation;
        private bool playing = false;


        // ------------------
        // Behaviour
        // ------------------
        public AnimatingSprite(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFramesPerSecond)
            : base(newTexture) // Passes the information up to the parent (base) class (Sprite).
        {
            // AnimatingSprite STUFF
            frameWidth = newFrameWidth;
            frameHeight = newFrameHeight;
            framesPerSecond = newFramesPerSecond;
        }
        // ------------------
        public void AddAnimation(string name, int startFrame, int endFrame)
        {
            Animation newAnimation = new Animation();
            newAnimation.startFrame = startFrame;
            newAnimation.endFrame = endFrame;
            animations.Add(name, newAnimation);
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                // Draw animating sprite!

                int numColumns = texture.Width / frameWidth;

                int xFrameIndex = currentFrame % numColumns; // the remainder when dividing.
                int yFrameIndex = currentFrame / numColumns; // rounds down (ignore remainder)

                Rectangle source = new Rectangle(xFrameIndex * frameWidth, yFrameIndex * frameHeight, frameWidth, frameHeight);

                spriteBatch.Draw(texture, position, source, Color.White);

            }
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            // Don't update if we aren't actually playing
            if (playing == false)
                return;

            float timeSinceLastUpdate = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Add to how long we've been in this frame
            timeInFrame += timeSinceLastUpdate;

            // Determine if it's time for a new frame
            // Calculate how long a frame SHOULD last
            float timePerFrame = 1.0f / framesPerSecond; // seconds per frame (inverse of frames per second)
            if (timeInFrame >= timePerFrame)
            {
                ++currentFrame;
                timeInFrame = 0;

                // Determine if we should reset our current animation
                Animation thisAnimation = animations[currentAnimation];

                if (currentFrame > thisAnimation.endFrame)
                {
                    // Reset our animation back to the beginning frame
                    currentFrame = thisAnimation.startFrame;
                }
            }
        }
        // ------------------
        public void PlayAnimation(string name)
        {
            // Don't reset the animation if we're already playing it
            if (name == currentAnimation && playing == true)
                return;

            // Error checking - is this animation even set up in the dictionary?
            bool animationIsSetup = animations.ContainsKey(name);
            Debug.Assert(animationIsSetup, "Animation named " + name + " not found in animating sprite.");

            // Only change animations IF the chosen animation is actually setup
            if (animationIsSetup)
            {
                // Record the name of our current animation
                currentAnimation = name;
                // Set our current frame to the first frame of our current animation
                currentFrame = animations[name].startFrame;
                // We just reset and started a new frame, so set frame time to 0
                timeInFrame = 0;
                // Start actually playing the animation
                playing = true;
            }
        }
        // ------------------
        public void StopAnimation()
        {
            playing = false;

            // Reset to the first frame of the animation
            currentFrame = animations[currentAnimation].startFrame;
        }
        // ------------------
        public override Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHeight);
        }
        // ------------------
    }
}
